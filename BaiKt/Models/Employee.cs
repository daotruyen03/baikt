﻿using System.ComponentModel.DataAnnotations;

namespace BaiKt.Models
{
    public class Employee
    {
        
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string ContactandAddress { get; set; }
        public string UsernameandPassword { get; set; }
    }
}
